<?php
/**
 * Jetpack Compatibility File
 * See: http://jetpack.me/
 *
 * @package FloraCC
 */

/**
 * Add theme support for Infinite Scroll.
 * See: http://jetpack.me/support/infinite-scroll/
 */
function flora_jetpack_setup() {
	add_theme_support( 'infinite-scroll', array(
		'container' => 'main',
		'render'    => 'flora_infinite_scroll_render',
		'footer'    => 'page',
	) );
} // end function flora_jetpack_setup
add_action( 'after_setup_theme', 'flora_jetpack_setup' );

function flora_infinite_scroll_render() {
	while ( have_posts() ) {
		the_post();
		get_template_part( 'template-parts/content', get_post_format() );
	}
} // end function flora_infinite_scroll_render