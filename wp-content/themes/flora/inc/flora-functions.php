<?php

/**
 * Sub Nav
 *
 * Generates a sidebar that lists child pages of the current top-level page.
 */
function flora_sub_nav() {
	global $post;
	$parent = $post->post_parent;
	
	// Is this a child page?
	if( $parent != 0 ) {
		// Yes. Get siblings.
		$children = get_children( array( 
			"post_parent" => $parent,
			"post_type" => "page",
			"post_status" => "publish",
			"orderby" => "menu_order",
			"order" => "ASC" 
		) );
		
		// If there are any children, create the menu
		if( $children ) {
			?>
			<nav class="sub-navigation paper paper-edge-both">
				<ul class="">
				<?php
				foreach( $children as $child ) {
					$active_class = "";
					if( $child->ID == $post->ID ) {
						$active_class = "active";
					}
					?><li class="<?php echo $active_class; ?>"><a href="<?php echo esc_url( get_the_permalink( $child->ID ) ) ?>"><?php echo esc_html( $child->post_title ); ?></a></li><?php
				}
				?>
				</ul>
			</nav>
			<?php
		}
	}
}

/**
 * Page class
 *
 * Adds special classes to the page container
 */
function flora_page_class() {
	/**
	 * Has sub nav class
	 */
	global $post;
	$parent = $post->post_parent;
	
	// Is this a child page?
	if( $parent != 0 ) {
		// Yes. Get siblings.
		$children = get_children( array( "post_parent" => $parent, "orderby" => "menu_order" ) );
		
		// If there are any children, create the menu
		if( $children ) {
			return " has-sub-nav ";
		}
	}
	
	return " no-sub-nav ";
}

/**
 * Member category
 *
 * Returns the first member category associated with a post.
 */
function flora_member_category() {
	$terms = get_the_terms( get_the_ID(), "member-categories" );
	return $terms[0]->name;
}

/**
 * Get members
 *
 * Returns JSON representation of all members
 */
function flora_get_members() {
	global $post;
	$args = array(
		"post_type" => "member",
		"numberposts" => -1,
		"orderby" => "title",
		"order" => "ASC",
	);
	
	$members = get_posts( $args );
	
	if( $members ):
		$member_cards = [];
		foreach( $members as $post ): setup_postdata( $post );
			$card = array(
				"title" => get_the_title(),
				"logo" => get_field( "member_logo" )["sizes"]["large"],
				"category" => flora_member_category(),
				"address" => wp_kses_post( get_field( "address" ) ),
				"phone" => wp_kses_post( get_field( "phone" ) ),
				"website" => addhttp( wp_kses_post( get_field( "website" ) ) ),
				"contact_name" => wp_kses_post( get_field( "contact_name" ) ),
				"email" => wp_kses_post( get_field( "email" ) ),
				"hours" => wp_kses_post( get_field( "hours" ) ),
			);
			$member_cards[] = $card;
		endforeach;
		return $member_cards;
	endif;
}

/**
 * Member categories
 *
 * Returns an array of all the active member categories
 */
function flora_member_categories() {
	return get_terms( array(
		"taxonomy" => "member-categories",
		"fields" => "names",
		"orderby" => "name",
		"order" => "ASC",
	) );
}

/**
 * Member directory callback
 *
 * Supplies the front-end with member directory data
 */
function flora_member_directory() {
	$response = array();
	$members = flora_get_members();
	$categories = flora_member_categories();
	
	if( $members ) {
		$response["members"] = $members;
	} else {
		$response["members"] = false;
	}
	
	if( $categories ) {
		$response["categories"] = $categories;
	} else {
		$response["categories"] = false;
	}
	
	echo json_encode( $response );
	
	wp_die();
}
add_action( "wp_ajax_nopriv_member_directory", "flora_member_directory" );
add_action( "wp_ajax_member_directory", "flora_member_directory" );

function temp_content() {

    return "<p class=\"lead\">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Exercitationem adipisci, consequatur, officiis veritatis, sed repudiandae excepturi itaque quam, fugiat accusamus voluptates quae quos ex soluta?</p>
        <h2>Subtitle</h2>
        <p>Consectetur adipisicing elit. Mollitia, maiores, perferendis eius porro commodi ullam laborum nesciunt iure ab dolorem. Dolorum, iure, omnis, dicta fugiat aliquam inventore officiis illum obcaecati culpa perferendis fuga numquam excepturi vero exercitationem praesentium. Ipsum, suscipit, expedita sapiente incidunt odit provident nisi tempore labore fuga harum aliquam ea dicta illo earum nesciunt maxime in corrupti a velit beatae adipisci veniam sed esse ducimus nemo ipsa quibusdam tenetur debitis minus officia cumque dolorem nulla quo temporibus fugit deserunt aspernatur vel. Tenetur, iure, sit deleniti sapiente enim cum laborum alias harum mollitia modi quidem aliquam error fuga autem?</p>
        <p>Consectetur adipisicing elit. Expedita, quisquam dolorum sed vitae quo qui veritatis in tempore ad facere corporis quas reprehenderit iure optio possimus perspiciatis sunt nesciunt hic. Aperiam, facere, eveniet mollitia dignissimos esse autem obcaecati vero enim dolor a minus error explicabo maxime aut fugiat ipsam labore deleniti quos ex similique assumenda cupiditate laboriosam itaque aliquid fuga. Tempora, expedita, labore, aut repellendus unde ipsum ex eius velit rem quae enim iusto suscipit.</p>
        <h3>Sub-Subtitle</h3>
        <p>Amet, consectetur adipisicing elit. Natus, sequi laudantium vitae ipsam eos perferendis maxime quisquam aspernatur illo cupiditate architecto itaque enim voluptates amet cumque cum rerum voluptatum. Rerum, molestiae, impedit, nobis, similique autem voluptatem vitae aliquid id libero deleniti illum placeat sed deserunt dolore eveniet molestias sunt quisquam itaque minima quas nesciunt accusamus culpa. Aperiam, unde illo accusamus commodi molestias fugit enim cum? Minus, dicta, illum reiciendis ea ab explicabo cumque sequi repellat debitis fugiat libero nostrum doloremque eaque eligendi qui ratione laudantium quis ullam dolorem at quibusdam!</p>";

}
add_shortcode( 'content', 'temp_content' );

/*
add_filter( 'the_content', 'empty_content' );

function empty_content( $content ) {
    if ( is_singular('page') && empty($content)) {
        $temp_content = temp_content();

        $content = $temp_content . $content;

    }

    return $content;
}
*/


/* !-- Helpers --------------------------------------------------------- */

function addhttp($url) {
    if (!preg_match("~^(?:f|ht)tps?://~i", $url) && $url != "" ) {
        $url = "http://" . $url;
    }
    return $url;
}