<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after
 *
 * @package FloraCC
 */
?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer row reversed" role="contentinfo">
		<div class="inner">
			<div class="site-info footer-module">
				<img class="footer-logo" src="<?php echo get_template_directory_uri(); ?>/images/logo.png" />
				<div class="contact-info">
					<p>P.O. Box 211<br />
						Flora, MS 39071</p>
					<p>(601) 879-9376</p>
					<p><a href="mailto:info@florachamber.net">info@florachamber.net</a></p>
					<div class="social-icons">
						<a class="social-icon facebook" href="https://www.facebook.com/faccms"><span class="visuallyhidden">Facebook</span></a>
					</div>
				</div>
				<div class="copyright">
					<p>&copy;<?php echo date("Y"); ?> Flora Chamber of Commerce<br />
						Website by <a href="http://madg.com/">Mad Genius</a>.</p>
				</div>
			</div>
<!--
			<div class="engagement footer-module">
				<div class="newsletter">
					<h1 class="h2">Sign up for our newsletter</h1>
					<?php echo do_shortcode('[gravityform id="1" title="false" description="false"]'); ?>
				</div>
			</div>
-->
			<div class="footer-nav footer-module">
				<?php wp_nav_menu( array( 'theme_location' => 'footer', "depth" => 1 ) ); ?>
			</div>
		</div>
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
