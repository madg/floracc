<?php
/**
 * The template for displaying the home page.
 *
 * @package FloraCC
 */

get_header(); ?>

<?php
	global $posts;
	$args = array(
		"post_type" => "member",
		"numberposts" => -1,
		"orderby" => "title",
		"order" => "ASC",
	);
	
	$members = get_posts( $args );
?>
	
<div class="row">
	<div class="inner">
		
	<?php while( have_posts() ): the_post(); ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		
		<?php if( get_the_content() ): ?>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		<?php endif; ?>
		
		<div class="members-filter">Member filter</div>
		
		<?php if( $members ): ?>
			<div class="members-list">
				<div class="grid-sizer"></div>
				<?php foreach( $members as $post ): setup_postdata( $post ); ?>
					<article class="member-card">
						<header class="member-header">
							<h1 class="member-title"><?php the_title(); ?></h1>
							<p class="member-category"><?php echo flora_member_category(); ?></p>
						</header>
						
						<?php if( get_field( "address" ) ): ?>
							<h3 class="h-field">Address</h3>
							<p class="member-address member-field"><?php echo wp_kses_post( get_field( "address" ) ); ?></p>
						<?php endif; ?>
						
						<?php if( get_field( "phone" ) ): ?>
							<h3 class="h-field">Phone</h3>
							<p class="member-phone member-field"><?php echo wp_kses_post( get_field( "phone" ) ); ?></p>
						<?php endif; ?>
						
						<?php if( get_field( "website" ) ): ?>
							<h3 class="h-field">Website</h3>
							<p class="member-website member-field"><?php echo wp_kses_post( get_field( "website" ) ); ?></p>
						<?php endif; ?>
						
						<div class="extra-fields">
							<?php if( get_field( "contact_name" ) ): ?>
								<h3 class="h-field">Contact Name</h3>
								<p class="member-contact member-field"><?php echo wp_kses_post( get_field( "contact_name" ) ); ?></p>
							<?php endif; ?>
							
							<?php if( get_field( "email" ) ): ?>
								<h3 class="h-field">Email</h3>
								<p class="member-email member-field"><?php echo wp_kses_post( get_field( "email" ) ); ?></p>
							<?php endif; ?>
							
							<?php if( get_field( "hours" ) ): ?>
								<h3 class="h-field">Hours</h3>
								<p class="member-email member-field"><?php echo wp_kses_post( get_field( "hours" ) ); ?></p>
							<?php endif; ?>
						</div>
					</article>
				<?php endforeach; ?>
			</div>
		<?php else: ?>
			<p>There is currently a problem displaying member information.</p>
		<?php endif; ?>
	<?php endwhile; ?>
	
	</div>
</div>
	
<?php get_footer(); ?>