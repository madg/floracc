(function($) {
	
	var members = [];
	var categories = [];
	var $grid = "";
	
	function MemberCard() {
		var self = this;
		
		self.title = "";
		self.category = "";
		self.address = "";
		self.phone = "";
		self.website = "";
		self.contact_name = "";
		self.email = "";
		self.hours = "";
	}
	
	function MemberViewModel() {
		var self = this;
		
		self.members = ko.observableArray(members);
		self.categories = ko.observableArray(categories);
		self.selectedCategory = ko.observable();
		
		self.filteredMembers = ko.computed(function() {
			if(self.selectedCategory()) {
				return self.members().filter(function(item) {
					return item.category === self.selectedCategoryLabel();
				});
			} else {
				self.members(members);
				return self.members();
			}
		});
		
		self.selectedCategoryLabel = ko.computed(function() {
			if(self.selectedCategory()) {
				return self.selectedCategory();
			}
		});
		
		self.layout = function() {
			// Reload Masonry and re-bind "show more" toggle button
			$.when($grid.masonry('reloadItems').masonry('layout')).then(function() {
				$(".member-more a").unbind();
				$(".member-more a").click(function() {
					var $a = $(this)
					var $p = $a.closest(".member-more");
					$p.toggleClass("opened");
					var label = "Show more";
					if($a.html() === "Show more") {
						label = "Show less";
					}
					$a.html(label);
					$(this).closest(".member-card").find(".extra-fields").slideToggle(400, function() {
						$grid.masonry('layout');
					});
				});
			});
		};
	}

	// When the document is ready, do stuff
	$(document).ready(function() {
		$grid = $(".members-list");
		
		// Get "member" posts from WordPress
		$.ajax({
			url: "/wp-admin/admin-ajax.php",
			type: "post",
			data: {
				action: "member_directory"
			}
		}).done(function(response) {
			if(response) {
				response = JSON.parse(response);
				members = response.members;
				categories = response.categories;
				console.log(categories);
			}
			
			// Initialize Knockout
			ko.applyBindings(new MemberViewModel());
			
			// Initialize Masonry after all logo images load
			$grid.imagesLoaded(function() {
				$grid.masonry({
					itemSelector: ".member-card",
					columnWidth: ".grid-sizer",
					gutter: ".gutter-sizer",
					percentPosition: "true",
				});
			});
			
			// Bind the "show more" toggle button
			$(".member-more a").click(function() {
				var $a = $(this)
				var $p = $a.closest(".member-more");
				$p.toggleClass("opened");
				var label = "Show more";
				if($a.html() === "Show more") {
					label = "Show less";
				}
				$a.html(label);
				$(this).closest(".member-card").find(".extra-fields").slideToggle(400, function() {
					$grid.masonry('layout');
				});
			});
		});
	});

})(jQuery);


// Decode HTML entities
var decodeEntities = (function() {
  // this prevents any overhead from creating the object each time
  var element = document.createElement('div');

  function decodeHTMLEntities (str) {
    if(str && typeof str === 'string') {
      // strip script/html tags
      str = str.replace(/<script[^>]*>([\S\s]*?)<\/script>/gmi, '');
      str = str.replace(/<\/?\w(?:[^"'>]|"[^"]*"|'[^']*')*>/gmi, '');
      element.innerHTML = str;
      str = element.textContent;
      element.textContent = '';
    }

    return str;
  }

  return decodeHTMLEntities;
})();