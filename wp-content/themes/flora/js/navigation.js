( function() {
	jQuery( ".menu-toggle" ).toggle( function() {
		jQuery( ".main-navigation" ).toggleClass( "is-open" );
		jQuery( "#page" ).animate( { "left": "-85%" }, 180, "swing", function() {
			jQuery( ".menu-toggle" ).toggleClass( "close" );
		} );
	}, function() {
		jQuery( ".main-navigation" ).toggleClass( "is-open" );
		jQuery( "#page" ).animate( { "left": "0" }, 180, "swing", function() {
			jQuery( ".menu-toggle" ).toggleClass( "close" );
		} );
	} );
} )();