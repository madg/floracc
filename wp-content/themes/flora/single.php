<?php
/**
 * The template for displaying a single blog/news post.
 *
 * @package FloraCC
 */

get_header(); ?>

<div class="row">
	<div class="inner">
		
		<div id="primary" class="content-area <?php echo flora_page_class(); ?>">
			<main id="main" class="site-main" role="main">
	
				<?php while ( have_posts() ) : the_post(); ?>
				
					<h1 class="h-page arc">News</h1>
					
					<?php get_template_part( 'template-parts/content' ); ?>
					
					<p><a href="<?php echo home_url( "/news" ) ?>">&larr; Back to News</a></p>
	
					<?php
						// If comments are open or we have at least one comment, load up the comment template
/*
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
*/
					?>
	
				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	
		<?php flora_sub_nav(); ?>
		
	</div>
</div>

<?php get_footer(); ?>
