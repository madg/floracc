<?php
/**
 * Template name: Member Directory
 *
 * The template for displaying the home page.
 *
 * @package FloraCC
 */

get_header(); ?>
	
<div class="row">
	<div class="inner" role="main">
		
	<?php while( have_posts() ): the_post(); ?>
		<h1 class="entry-title"><?php the_title(); ?></h1>
		
		<?php if( get_the_content() ): ?>
			<div class="entry-content">
				<?php the_content(); ?>
			</div>
		<?php endif; ?>
		
		<div class="members-filter">
			<p>Select a category or browse our full directory of members:
			<select data-bind="
				options: categories,
				optionsText: function(item) {
					return decodeEntities(item);
				},
				value: selectedCategory,
				optionsCaption: 'All',
				event: { change: layout }"></select></p>
			<!-- <p data-bind="html: selectedCategoryLabel, visible: selectedCategoryLabel"></p> -->
		</div>
		
		<div class="members-list" data-bind="if: filteredMembers">
			<div class="grid-sizer"></div>
			<div class="gutter-sizer"></div>
			<!-- ko foreach: filteredMembers -->
				<article class="member-card">
					<div class="member-logo" data-bind="visible: logo">
						<img data-bind="attr: { src: logo }" />
					</div>
					
					<header class="member-header">
						<h1 class="member-title" data-bind="html: title"></h1>
						<p class="member-category" data-bind="html: category"></p>
					</header>
					
					<h3 class="h-field" data-bind="visible: address">Address</h3>
					<p class="member-address member-field" data-bind="html: address, visible: address"></p>
					
					<h3 class="h-field" data-bind="visible: phone">Phone</h3>
					<p class="member-phone member-field" data-bind="html: phone, visible: phone"></p>
					
					<h3 class="h-field" data-bind="visible: website">Website</h3>
					<p class="member-website member-field" data-bind="visible: website"><a data-bind="html: website, attr: { href: website }"></a></p>
					
					<div class="extra-fields">
						<h3 class="h-field" data-bind="visible: contact_name">Contact Name</h3>
						<p class="member-contact member-field" data-bind="html: contact_name, visible: contact_name"></p>
						
						<h3 class="h-field" data-bind="visible: email">Email</h3>
						<p class="member-email member-field" data-bind="html: email, visible: email"></p>
						
						<h3 class="h-field" data-bind="visible: hours">Hours</h3>
						<p class="member-hours member-field" data-bind="html: hours, visible: hours"></p>
					</div>
					
					<p class="member-more" data-bind="visible: contact_name || email || hours"><a>Show more</a></p>
				</article>
			<!-- /ko -->
		</div>
	<?php endwhile; ?>
	
	</div>
</div>
	
<?php get_footer(); ?>