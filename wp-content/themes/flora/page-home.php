<?php
/**
 * The template for displaying the home page.
 *
 * @package FloraCC
 */

get_header(); ?>

<?php while ( have_posts() ) : the_post(); ?>

	<?php get_template_part( "template-parts/home", "intro" ); ?>
	
	<?php get_template_part( "template-parts/home", "spotlight" ); ?>
	
	<div class="row">
		<div class="inner">
			
			<?php get_template_part( "template-parts/home", "news" ); ?>
			
			<?php get_template_part( "template-parts/home", "events" ) ?>
			
		</div>
	</div>
	
<?php endwhile; // end of the loop. ?>

<?php get_footer(); ?>