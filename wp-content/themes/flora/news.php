<?php
/**
 * Template name: News
 *
 * The template for displaying blog/news posts.
 *
 * @package FloraCC
 */

get_header(); ?>

<div class="row">
	<div class="inner">
		
		<div id="primary" class="content-area <?php echo flora_page_class(); ?>">
			<main id="main" class="site-main" role="main">
	
				<?php while ( have_posts() ) : the_post(); ?>
				
					<h1 class="h-page"><?php the_title(); ?></h1>
					
					<?php
						global $wp_query;
						$temp = $wp_query;
						
						$wp_query = new WP_Query( array(
							"post_type" => "post",
							"posts_per_page" => 5,
						) );
						
						if( $wp_query->have_posts() ) :
							while( $wp_query->have_posts() ) : $wp_query->the_post();
								get_template_part( 'template-parts/content' );
							endwhile;
							
							the_posts_navigation();
						endif;
						
						$wp_query = $temp;
						wp_reset_postdata();
					?>
	
					<?php
						// If comments are open or we have at least one comment, load up the comment template
/*
						if ( comments_open() || get_comments_number() ) :
							comments_template();
						endif;
*/
					?>
	
				<?php endwhile; // end of the loop. ?>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	
		<?php flora_sub_nav(); ?>
		
	</div>
</div>

<?php get_footer(); ?>
