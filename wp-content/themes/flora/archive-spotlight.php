<?php
/**
 * The template for displaying member spotlights (custom post type).
 *
 * @package FloraCC
 */

get_header(); ?>

<div class="row">
	<div class="inner">
		
		<div id="primary" class="content-area <?php echo flora_page_class(); ?>">
			<main id="main" class="site-main" role="main">
				
				<h1 class="h-page">Member Spotlight</h1>
				
				<?php
					global $wp_query;
					$temp = $wp_query;
					
					$wp_query = new WP_Query( array(
						"post_type" => "spotlight",
						"posts_per_page" => 5,
					) );
					
					if( $wp_query->have_posts() ) :
						while( $wp_query->have_posts() ) : $wp_query->the_post();
							get_template_part( 'template-parts/content' );
						endwhile;
						
						the_posts_navigation();
					endif;
					
					$wp_query = $temp;
					wp_reset_postdata();
				?>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	
		<?php flora_sub_nav(); ?>
		
	</div>
</div>

<?php get_footer(); ?>
