<?php
/**
 * Template name: Events Page
 *
 * The template for displaying all upcoming events.
 *
 * @package FloraCC
 */

get_header(); ?>

<?php
	global $post;
	
	$args = array(
		"post_type" => "event",
		"posts_per_page" => -1,
		"meta_query" => array(
			array(
				"key" => "tba",
				"value" => "0",
			),
		),
		/* "meta_key" => "start_datetime",
		"orderby" => "meta_value_num", */
		"order" => "DESC",
		"post_status" => "publish"
	);
	$events = get_posts( $args );
	
	$args = array(
		"post_type" => "event",
		"posts_per_page" => -1,
		"meta_query" => array(
			array(
				"key" => "tba",
				"value" => "1",
			),
		),
		"meta_key" => "custom_date_label",
		"orderby" => "meta_value",
		"order" => "ASC",
	);
	$tba_events = get_posts( $args );
?>

<div class="row content-area <?php echo flora_page_class(); ?>">
	<div class="inner site-main" role="main">
	
	<h1 class="h-page">Upcoming Events</h1>
	
	<div class="events-list">
	<?php foreach( $events as $post ): setup_postdata( $post );
		$event_unix_starttime = strtotime(get_field('start_datetime'));
		if ($event_unix_starttime > strtotime('now')) {
			get_template_part( "template-parts/event", "content" );
		}
	endforeach; ?>
	
	<?php foreach( $tba_events as $post ): setup_postdata( $post ); ?>
		
		<?php get_template_part( "template-parts/event", "content" ); ?>
		
	<?php endforeach; ?>
	</div>
		
	</div>
</div>

<?php get_footer(); ?>
