<?php
/**
 * The template for displaying 404 pages (not found).
 *
 * @package FloraCC
 */

get_header(); ?>

<div class="row">
	<div class="inner">
		
		<div id="primary" class="content-area <?php echo flora_page_class(); ?>">
			<main id="main" class="site-main" role="main">
	
				<h1 class="entry-title">Page not found</h1>
				
				<p>Even in a small town, things get lost or misplaced. We can’t seem
					to find the page you're looking for. It could be that the page
					has moved. Or maybe there’s a mis-type in the URL. Try using
					the site navigation menu above to find what you are looking for.
					Otherwise, head over to the <a href="<?php echo home_url( "/" ); ?>">home page</a>.
					We put the latest information there.</p>
					
				<p>If you still don’t find what you need, please give us a call at
					(601) 879-9376 and we’ll do our best to help.</p>
	
			</main><!-- #main -->
		</div><!-- #primary -->
	
		<?php flora_sub_nav(); ?>
		
	</div>
</div>

<?php get_footer(); ?>
