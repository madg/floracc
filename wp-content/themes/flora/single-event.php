<?php get_header(); ?>

<div class="row content-area <?php echo flora_page_class(); ?>">
	<div class="inner site-main" role="main">
	
	<h1 class="h-page">Upcoming Events</h1>
	
	<div class="events-list">
		
		<?php while( have_posts() ): the_post(); ?>
		
			<?php get_template_part( "template-parts/event", "content" ); ?>
		
		<?php endwhile; ?>
		
		<p><a href="<?php echo home_url( "/events" ); ?>">&larr; See all events</a></p>
		
	</div>
		
	</div>
</div>

<?php get_footer(); ?>