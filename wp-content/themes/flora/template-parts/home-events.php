<?php
	global $post;
	
	$args = array(
		"post_type" => "event",
		"posts_per_page" => 5,
		"meta_query" => array(
			array(
				"key" => "tba",
				"value" => "0",
			)
		),
		//"meta_key" => "start_datetime",
		//"orderby" => "meta_value_num",
		"post_status" => "publish",
		"order" => "DESC",
	);
	$events = get_posts( $args );
	//var_dump($events);
	$args = array(
		"post_type" => "event",
		"posts_per_page" => 3,
		"meta_query" => array(
			array(
				"key" => "tba",
				"value" => "1",
			),
		),
		"meta_key" => "custom_date_label",
		"orderby" => "meta_value",
		"order" => "ASC",
	);
	$tba_events = get_posts( $args );
?>

<div class="home-sidebar paper paper-edge-both">
	<h1 class="home-sidebar-title">Events</h1>
	<ul class="home-events-list">
	<?php // Events that are not TBA ?>
	<?php foreach( $events as $post ): setup_postdata( $post ); 
	if(strtotime(get_field('start_datetime')) > strtotime('now')):
	?>
		<li>
			<h2 class="event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="event-meta">
				<?php
					if( get_field( "start_datetime" ) ) {
						$datetime = get_field( "start_datetime" );
						$datetime = new DateTime( $datetime );
						if( get_field( "all_day_event" ) ) {
							?><p class="event-duration"><?php echo $datetime->format( "M j, Y" ); ?></p><?php
						} else {
							?><p class="event-duration"><?php echo $datetime->format( "M j, Y" ) . " <em>at</em> " . $datetime->format( "g:ia" ); ?></p><?php
						}
					}
					if( get_field( "location" ) ) {
						?><p class="event-location"><?php echo wp_kses( get_field( "location" ), wp_kses_allowed_html( "post" ) ); ?></p><?php
					}
				?>
			</div>
		</li>
	<?php endif; ?>
	<?php endforeach; ?>
	
	<?php // Events that *are* TBA ?>
	<?php foreach( $tba_events as $post ): setup_postdata( $post ); ?>
		<li>
			<h2 class="event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
			<div class="event-meta">
				<?php echo esc_html( get_field( "custom_date_label" ) ); ?>
			</div>
		</li>
	<?php endforeach; ?>
	
	<p><a href="<?php echo home_url( "/events" ) ?>">See all events &rarr;</a></p>
	</ul>
</div>
