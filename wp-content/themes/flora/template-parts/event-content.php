<article class="event">
	<h2 class="event-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h2>
	<div class="event-meta">
		<?php
			if( get_field( "start_datetime" ) ) {
				$datetime = get_field( "start_datetime" );
				$datetime = new DateTime( $datetime );
				if( get_field( "all_day_event" ) ) {
					?><p class="event-duration"><?php echo $datetime->format( "M j, Y" ); ?></p><?php
				} else {
					?><p class="event-duration"><?php echo $datetime->format( "M j, Y" ) . " <em>at</em> " . $datetime->format( "g:ia" ); ?></p><?php
				}
			}
			if( get_field( "location" ) ) {
				?><p class="event-location"><?php echo wp_kses( get_field( "location" ), wp_kses_allowed_html( "post" ) ); ?></p><?php
			}		?>
	</div>
	
	<?php if( has_post_thumbnail() ): ?>
	<div class="featured-image-wrapper">
		<div class="featured-image-inner">
			<?php the_post_thumbnail(); ?>
		</div>
	</div>
	<?php endif; ?>
	
	<div class="event-content">
		<?php the_content(); ?>
		<?php
			if( get_field( "facebook_event_url" ) ) {
				?><p class="event-facebook"><a href="<?php echo esc_url( get_field( "facebook_event_url" ) ); ?>">Join the event on Facebook &rarr;</a></p><?php
			}
		?>
	</div>
</article>