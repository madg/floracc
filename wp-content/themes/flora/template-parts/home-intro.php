<?php
/**
 * The template used for displaying page content on the home page
 *
 * @package FloraCC
 */
?>

<?php if( get_field( "intro_text" ) || get_field( "has_button" ) ): ?>

	<div class="home-intro">
		<div class="inner">
			
			<?php if( get_field( "intro_text" ) ): ?>
			<p class="home-intro-label">
				<?php echo wp_kses_post( get_field( "intro_text" ) ); ?>
			</p>
			<?php endif; ?>
			
			<?php if( get_field( "has_button" ) ): ?>
			<p class="home-intro-button">
				<a href="<?php echo esc_url( get_field( "intro_button_link" ) ); ?>">
					<?php echo wp_kses_post( get_field( "intro_button_label" ) ); ?>
				</a>
			</p>
			<?php endif; ?>
			
		</div>
	</div>
	
<?php endif; ?>