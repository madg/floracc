<?php
	global $post;
	$args = array(
		"post_type" => "post",
		"orderby" => "date",
		"order" => "DESC",
		"posts_per_page" => 3
	);
	$news_posts = get_posts( $args );
?>
<?php if( $news_posts ): ?>
	
	<div class="home-news">
		
		<h2 class="h1">Latest News</h2>

	<?php foreach( $news_posts as $post ): setup_postdata( $post ); ?>
	
		<article class="news-post">
			
			<?php if( has_post_thumbnail() ): ?>
				<div class="news-featured-image">
					<?php the_post_thumbnail( "thumbnail" ); ?>
				</div>
			<?php endif; ?>
			
			<div class="news-content <?php if( !has_post_thumbnail() ) echo "no-image"; ?>">
				<h1 class="h-news-title"><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></h1>
				<p class="news-posted-on">Posted on <?php the_time( "M j, Y " ); ?></p>
				<?php the_excerpt(); ?>
			</div>
			
		</article>
	
	<?php endforeach; ?>
	
	<p><a class="btn-link" href="<?php echo home_url( "/news" ); ?>">Get more news</a></p>
			
	</div>

<?php endif; ?>