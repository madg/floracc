<?php
	global $post;
	$args = array(
		"post_type" => "spotlight",
		"orderby" => "date",
		"order" => "DESC",
		"posts_per_page" => 1
	);
	$spotlight_posts = get_posts( $args );
?>
<?php if( $spotlight_posts ): ?>

	<?php foreach( $spotlight_posts as $post ): setup_postdata( $post ); ?>
	
		<article class="home-spotlight row">
			<div class="inner">
				
				<div class="home-spotlight-photo featured-image-wrapper">
					<div class="featured-image-inner">
						<?php the_post_thumbnail(); ?>
					</div>
				</div>
				
				<div class="home-spotlight-content">
					<h1 class="h-spotlight"><span>Spotlight on</span> <?php the_title(); ?></h1>
					<?php the_excerpt(); ?>
					<p class="btn-wrap"><a class="btn-link-reverse" href="<?php the_permalink(); ?>">Read more</a></p>
				</div>
				
			</div>
		</article>
	
	<?php endforeach; ?>

<?php endif; ?>