<?php
/**
 * The template used for displaying page content in page.php
 *
 * @package FloraCC
 */
?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>
	<header class="entry-header">
		<?php the_title( '<h1 class="entry-title">', '</h1>' ); ?>
	</header><!-- .entry-header -->
	
	<?php if( has_post_thumbnail() ): ?>
	<div class="featured-image-wrapper">
		<div class="featured-image-inner">
			<?php the_post_thumbnail(); ?>
		</div>
	</div>
	<?php endif; ?>

	<div class="entry-content">
		<?php the_content(); ?>
	</div><!-- .entry-content -->

	<footer class="entry-footer">
		<?php edit_post_link( __( 'Edit', 'flora' ), '<span class="edit-link">', '</span>' ); ?>
	</footer><!-- .entry-footer -->
</article><!-- #post-## -->
